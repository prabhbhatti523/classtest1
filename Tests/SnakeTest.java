import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SnakeTest {

	Snake s1;
	Snake s2;
	
	
	@Before
	public void setUp() throws Exception {
		// create two snakes
		
		s1 = new Snake("Peter", 10, "coffee");
		s2 = new Snake("Takis" , 80, "vegetables");
	}
	

	@After
	public void tearDown() throws Exception {
	}



	
	@Test
	public void TC1(){
		// s1 have coffee so expected is false
		
		assertEquals(false, s1.isHealthy());
		// s1 is unhealthy
		
		// snake 2 (s2) have vegetables , so expected result is true
		assertEquals(true, s2.isHealthy());
		// s2 is healthy
		
		
		
	}
	
	// do snake fits in the cage
	@Test
	public void TC2(){
		
		// done for snake s1
		
		// when cage length is less than snake length
		// expected outcome is false
		assertEquals(false, s1.fitsInCage(5));
		
		// when cage length is equal than snake length
				// expected outcome is false
		
		assertEquals(false, s1.fitsInCage(10));
		
		// when cage length is more than snake length
				// expected outcome is true
		
		
		assertEquals(true, s1.fitsInCage(15));
		
		
	}
	
}
