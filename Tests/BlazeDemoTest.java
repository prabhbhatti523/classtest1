import static org.junit.Assert.*;

import java.awt.List;
import java.lang.reflect.Array;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ByIdOrName;
import org.openqa.selenium.support.ui.Select;

public class BlazeDemoTest {


	
	 	final String CHROMEDRIVER_LOCATION = "/Users/macstudent/Desktop/chromedriver";
	 
	 	final String URL_TO_TEST = "http://blazedemo.com/";
	 	
	 	WebDriver driver;
		@Before
		public void setUp() throws Exception {
			
			 		System.setProperty("webdriver.chrome.driver", CHROMEDRIVER_LOCATION);
			 		driver = new ChromeDriver();
			 	
			 		driver.get(URL_TO_TEST);
		}

		@After
		public void tearDown() throws Exception {
			Thread.sleep(5000);
			driver.close();
			
		}

		@Test
		public void countDepartureCities() {
			WebElement element = driver.findElement(By.name("fromPort"));
			Select city = new Select(element);
			city.selectByIndex(1);
			// expected departure cities are 7
	       java.util.List<WebElement> listOptionDropdown =  city.getOptions();
	       assertEquals(listOptionDropdown.size(), 7);
	       //expected == actual
	       
			
		
			
		}

}
